import pytest
import json

from ska_csp_config import *

CSP_CONFIG = """
{
    "id": "sbi-mvp01-20200325-00001-science_A",
    "frequencyBand": "1",
    "fsp": [
      {
        "fspID": 1, "functionMode": "CORR", "frequencySliceID": 1,
        "integrationTime": 1400, "corrBandwidth": 0,
        "channelAveragingMap": [[0,2], [744,0]],
        "outputChannelOffset": 0,
        "outputLinkMap": [[0,0], [200,1]]
      },
      {
        "fspID": 2, "functionMode": "CORR", "frequencySliceID": 2,
        "integrationTime": 1400, "corrBandwidth": 0,
        "channelAveragingMap": [[0,2], [744,0]],
        "outputChannelOffset": 744,
        "outputLinkMap": [[0,4], [200,5]]
      }
    ]
}
"""

SDP_RECEIVE_ADDR_MAP = """
{
  "science_A": {
    "host": [[0, "192.168.0.1"], [400, "192.168.0.2"], [744, "192.168.0.3"], [1144, "192.168.0.4"]],
    "mac": [[0, "06-00-00-00-00-00"], [744, "06-00-00-00-00-01"]],
    "port": [[0, 9000, 1], [400, 9000, 1], [744, 9000, 1], [1144, 9000, 1]]
  },
  "calibration_A": {
    "host": [[0, "192.168.1.1"]],
    "port": [[0, 9000, 1]]
  }
}
"""


CSP_CONFIG_OUT_CAL_A = """
{
    "id": "sbi-mvp01-20200325-00001-science_A",
    "frequencyBand": "1",
    "fsp": [
      {
        "fspID": 1, "functionMode": "CORR", "frequencySliceID": 1,
        "integrationTime": 1400, "corrBandwidth": 0,
        "channelAveragingMap": [[0,2], [744,0]],
        "outputChannelOffset": 0,
        "outputLinkMap": [[0,0], [200,1]],
        "outputHost": [[0, "192.168.1.1"]],
        "outputPort": [[0, 9000, 1]]
      },
      {
        "fspID": 2, "functionMode": "CORR", "frequencySliceID": 2,
        "integrationTime": 1400, "corrBandwidth": 0,
        "channelAveragingMap": [[0,2], [744,0]],
        "outputChannelOffset": 744,
        "outputLinkMap": [[0,4], [200,5]],
        "outputHost": [[0, "192.168.1.1"]],
        "outputPort": [[0, 9744, 1]]
      }
    ]
  }
"""

CSP_CONFIG_OUT_SCIENCE_A = """
{
    "id": "sbi-mvp01-20200325-00001-science_A",
    "frequencyBand": "1",
    "fsp": [
      {
        "fspID": 1, "functionMode": "CORR", "frequencySliceID": 1,
        "integrationTime": 1400, "corrBandwidth": 0,
        "channelAveragingMap": [[0,2], [744,0]],
        "outputChannelOffset": 0,
        "outputLinkMap": [[0,0], [200,1]],
        "outputHost": [[0, "192.168.0.1"], [400, "192.168.0.2"]],
        "outputMac": [[0, "06-00-00-00-00-00"]],
        "outputPort": [[0, 9000, 1], [400, 9000, 1]]
      },
      {
        "fspID": 2, "functionMode": "CORR", "frequencySliceID": 2,
        "integrationTime": 1400, "corrBandwidth": 0,
        "channelAveragingMap": [[0,2], [744,0]],
        "outputChannelOffset": 744,
        "outputLinkMap": [[0,4], [200,5]],
        "outputHost": [[0, "192.168.0.3"], [400, "192.168.0.4"]],
        "outputMac": [[0, "06-00-00-00-00-01"]],
        "outputPort": [[0, 9000, 1], [400, 9000, 1]]
      }
    ]
}
"""

def _make_csp_config_no_off(scan_type, csp_config_json, receive_addrs_json):

    # Remove output channel offset
    csp_config_no_offsets = json.loads(csp_config_json)
    for fsp in csp_config_no_offsets['fsp']:
        del fsp['outputChannelOffset']

    # Removing offsets is going to move FSP 2 from the given offset
    # (744) to the default offset (14880). Adjust maps accordingly.
    recv_addr_no_offsets = json.loads(receive_addrs_json)
    for st in recv_addr_no_offsets.values():
        for ch_map in st.values():
            for entry in ch_map:
                if entry[0] >= 744:
                    entry[0] += (14880 - 744)

    return make_csp_config(scan_type, json.dumps(csp_config_no_offsets),
                           json.dumps(recv_addr_no_offsets))

def test_receive_addrs():

    # Check calibration_A
    csp_config_a = make_csp_config('calibration_A', CSP_CONFIG, SDP_RECEIVE_ADDR_MAP)
    assert json.loads(csp_config_a) == json.loads(CSP_CONFIG_OUT_CAL_A)

    # Check science_A
    csp_config_a2 = make_csp_config('science_A', CSP_CONFIG, SDP_RECEIVE_ADDR_MAP)
    assert json.loads(csp_config_a2) == json.loads(CSP_CONFIG_OUT_SCIENCE_A)

    # Check science_A if we remove the output channel offsets
    # (this is a bit hacky - this should eventually be removed anyway)
    csp_config_a3 = _make_csp_config_no_off('science_A', CSP_CONFIG, SDP_RECEIVE_ADDR_MAP)
    csp_config_expected = json.loads(CSP_CONFIG_OUT_SCIENCE_A)
    csp_config_expected['fsp'][1]['outputChannelOffset'] = 14880
    assert json.loads(csp_config_a3) == csp_config_expected

    # Check exceptions
    with pytest.raises(ValueError):
        make_csp_config('calibration_B', CSP_CONFIG, SDP_RECEIVE_ADDR_MAP)
