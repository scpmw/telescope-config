import pytest

from ska_csp_config.channel_map import *


def test_channel_map_at_basic():

    chan_map = [[0,0],[5,1],[7,0]]
    with pytest.raises(ValueError):
        channel_map_at([], 0)
    with pytest.raises(ValueError):
        channel_map_at(chan_map, -1)
    for i in range(5):
        assert channel_map_at(chan_map, i) == 0
        assert channel_map_at(chan_map, i, make_entry=True) == [i,0]
    for i in range(5,7):
        assert channel_map_at(chan_map, i) == 1
        assert channel_map_at(chan_map, i, make_entry=True) == [i,1]
    for i in range(7,10):
        assert channel_map_at(chan_map, i) == 0
        assert channel_map_at(chan_map, i, make_entry=True) == [i,0]

def test_channel_map_at_incr():

    # Map with increments
    chan_map = [[0,0,1],[5,5,-1],[7,0,1]]
    # Map where some elements do *not* have an increment
    chan_map_mix = [[0,0,1],[5,5],[7,0,1]]

    for cmap in [chan_map, chan_map_mix]:
        with pytest.raises(ValueError):
            channel_map_at(cmap, -1)
        for i in range(5):
            assert channel_map_at(cmap, i) == i
            assert channel_map_at(cmap, i, make_entry=True) == [i,i,1]
        for i in range(7,10):
            assert channel_map_at(cmap, i) == i - 7
            assert channel_map_at(cmap, i, make_entry=True) == [i,i-7,1]
    for i in range(5,7):
        assert channel_map_at(chan_map, i) == 10 - i
        assert channel_map_at(chan_map, i, make_entry=True) == [i,10-i,-1]
    for i in range(5,7):
        assert channel_map_at(chan_map_mix, i) == 5
        assert channel_map_at(chan_map_mix, i, make_entry=True) == [i,5]

def test_validate():

    with pytest.raises(ValueError):
        split_channel_map([], 0, 10)
    with pytest.raises(ValueError):
        split_channel_map([[1,0]], 0, 10)
    assert split_channel_map([[0,0]], 0, 10) == [[[0,0]]]
    with pytest.raises(ValueError):
        split_channel_map([[0,0], [0,0]], 0, 10)
    with pytest.raises(ValueError):
        split_channel_map([[0,0], [1,0], [1,0]], 0, 10)
    with pytest.raises(ValueError):
        split_channel_map([[0,0], [-1,0]], 0, 10)
    with pytest.raises(ValueError):
        split_channel_map([[0,0], [0,0], [-1,0]], 0, 10)

def test_channel_map_split():

    assert split_channel_map([[0,0]], 0, 10) == [[[0,0]]]
    assert split_channel_map([[0,0]], 0, 10, minimum_groups=0) == [[[0,0]]]
    assert split_channel_map([[0,0]], 0, 10, minimum_groups=1) == [[[0,0]]]
    assert split_channel_map([[0,0]], 0, 10, minimum_groups=2) == [[[0,0]], [[10,0]]]
    assert split_channel_map([[0,0]], 0, 10, minimum_groups=3) == [[[0,0]], [[10,0]], [[20,0]]]

    # Check some normal splits with different group sizes
    for ch0 in range(10):

        # Shift channel map to start channel, making sure it does the right thing
        cmap = shift_channel_map([[-1,0,1],[0,0,1],[5,5,-1],[7,0,1]], ch0)
        assert cmap[0][0] == ch0 - 1
        assert cmap[-1][0] == ch0 + 7


        # Test - shifting the references as appropriate as well
        def shift(maps):
            return [ shift_channel_map(m, ch0) for m in maps ]
        assert split_channel_map(cmap, ch0, 8) == shift([[[0,0,1],[5,5,-1], [7,0,1]]])
        assert split_channel_map(cmap, ch0, 7) == shift([[[0,0,1],[5,5,-1]], [[7,0,1]]])
        assert split_channel_map(cmap, ch0, 6) == shift([[[0,0,1],[5,5,-1]], [[6,4,-1], [7,0,1]]])
        assert split_channel_map(cmap, ch0, 5) == shift([[[0,0,1]], [[5,5,-1], [7,0,1]]])
        assert split_channel_map(cmap, ch0, 4) == shift([[[0,0,1]], [[4,4,1], [5,5,-1], [7,0,1]]])
        assert split_channel_map(cmap, ch0, 3) == shift([[[0,0,1]], [[3,3,1], [5,5,-1]],
                                                         [[6,4,-1], [7,0,1]]])

        expected = shift([[[0,0,1]], [[2,2,1]],[[4,4,1],[5,5,-1]], [[6,4,-1], [7,0,1]]])
        assert split_channel_map(cmap, ch0, 2) == expected
        assert split_channel_map(cmap, ch0, 2, minimum_groups=len(expected)) == expected
        assert split_channel_map(cmap, ch0, 2, minimum_groups=len(expected)+1) == expected + shift([[[8,1,1]]])
        assert split_channel_map(cmap, ch0, 2, minimum_groups=len(expected)+2) == expected + \
            shift([[[8,1,1]], [[10,3,1]]])

        # Make sure results are right if we rebase every group
        for rb in [0,1]:
            def shift(maps):
                return [ shift_channel_map(m, rb) for m in maps ]
            assert split_channel_map(cmap, ch0, 8, rb) == shift([[[0,0,1],[5,5,-1], [7,0,1]]])
            assert split_channel_map(cmap, ch0, 7, rb) == shift([[[0,0,1],[5,5,-1]], [[0,0,1]]])
            assert split_channel_map(cmap, ch0, 6, rb) == shift([[[0,0,1],[5,5,-1]], [[0,4,-1], [1,0,1]]])
            assert split_channel_map(cmap, ch0, 5, rb) == shift([[[0,0,1]], [[0,5,-1], [2,0,1]]])
            assert split_channel_map(cmap, ch0, 4, rb) == shift([[[0,0,1]], [[0,4,1], [1,5,-1], [3,0,1]]])
            assert split_channel_map(cmap, ch0, 3, rb) == shift([[[0,0,1]], [[0,3,1], [2,5,-1]],
                                                                 [[0,4,-1], [1,0,1]]])
            assert split_channel_map(cmap, ch0, 2, rb) == shift([[[0,0,1]], [[0,2,1]],[[0,4,1],[1,5,-1]],
                                                                 [[0,4,-1], [1,0,1]]])

    chan_map_mix = [[0,0,1],[5,5],[7,0,1]]
    assert split_channel_map(chan_map_mix, 0, 2) == [chan_map_mix[:1], [[2,2,1]],
                                                     [[4,4,1], [5,5]], [[6,5], [7,0,1]]]

def test_channel_map_split_at():

    with pytest.raises(ValueError):
        split_channel_map_at([], [0,10])
    with pytest.raises(ValueError):
        split_channel_map_at([[1,0]], [0, 10])

    assert split_channel_map_at([[0,0]], []) == []
    assert split_channel_map_at([[0,0]], [0]) == []
    assert split_channel_map_at([[0,0]], [0, 10]) == [[[0,0]]]
    assert split_channel_map_at([[0,0]], [0, 10]) == [[[0,0]]]
    assert split_channel_map_at([[0,0]], [0, 10, 20]) == [[[0,0]], [[10,0]]]
    assert split_channel_map_at([[0,0]], [0, 10, 20, 30]) == [[[0,0]], [[10,0]], [[20,0]]]

    # Attempt cutting out parts of an existing non-uniformely spaced channel map
    cmap = [[-10,3], [-1,0,1],[0,0,1],[5,5,-1],[7,0,1],[10,4]]
    for start in range(len(cmap)):
        for end in range(start, len(cmap)-1):
            assert split_channel_map_at(cmap, [ ch for ch, *_ in cmap[start:end+2] ]) == \
                [ [entry] for entry in cmap[start:end+1] ]
