
from . import channel_map

import logging
import json

LOGGER = logging.getLogger("ska_csp_telconfig")

# JSON names
FSP_ID = 'fspID'
FSP_CH_OFFSET = 'outputChannelOffset'

def get_fsp_channel_offset(csp_config_in:dict) -> int:
    """
    Determines first channel ID within an FSP
    """

    # Ditto?
    return 0


def get_fsp_output_channel_offset(fsp_config :dict) -> int:
    """
    Determines the FSP channel offset. Either read from the dictionary
    or reconstructed.

    :param fsp_config: FSP configuration structure
    """

    # Return channel offset if it is set
    if FSP_CH_OFFSET in fsp_config:
        return fsp_config[FSP_CH_OFFSET]

    # Otherwise fallback to calculation from FSP ID
    LOGGER.warning(f'FSP lacks output channel offset, generating: {fsp_config}')
    return 14880 * (fsp_config[FSP_ID] - 1)

def add_receive_addresses(scan_type :str, csp_config:dict, scan_receive_addrs:dict) -> dict:
    """
    Add SDP receive addresses into CSP configuration

    :param scan_trype: Scan type executed
    :param csp_config: CSP input configuration
    :param sdp_receive_addrs: SDP receive addresses for scan
    :returns: New CSP configuration
    """

    FSP = 'fsp'

    # Collect fsps, adding output offset as applicable
    for fsp in csp_config[FSP]:
        fsp[FSP_CH_OFFSET] = get_fsp_output_channel_offset(fsp)

    # Sort fsps by output channel offset, determining channel groups
    fsps_sorted = list(sorted(csp_config[FSP], key=lambda fsp: fsp[FSP_CH_OFFSET]))
    channel_groups = [ fsp[FSP_CH_OFFSET] for fsp in fsps_sorted ]

    # Determine channel maps to split
    MAPS_TO_SPLIT = {
        'host': 'outputHost',
        'mac': 'outputMac',
        'port': 'outputPort',
    }

    ch_offset = get_fsp_channel_offset(csp_config)
    for in_map_name, out_map_name in MAPS_TO_SPLIT.items():

        # Split map, if present
        if in_map_name not in scan_receive_addrs:
            continue
        in_map = scan_receive_addrs[in_map_name]
        groups = channel_groups + [in_map[-1][0]+1]
        split_map = channel_map.split_channel_map_at(in_map, groups, ch_offset)

        # Add to FSP sections
        for fsp, new_map in zip(csp_config[FSP], split_map):
            fsp[out_map_name] = new_map

    # Done
    return csp_config
