SKA Telescope Configuration
===========================

[![Documentation Status](https://readthedocs.org/projects/telescope-config/badge/?version=latest)](https://developer.skatelescope.org/projects/telescope-config/en/latest/?badge=latest)

Repository for handling SKA telescope configuration
